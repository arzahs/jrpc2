module bitbucket.org/arzahs/jrpc2

require (
	bitbucket.org/creachadair/shell v0.0.4
	bitbucket.org/creachadair/stringset v0.0.5
	github.com/kylelemons/godebug v0.0.0-20170820004349-d65d576e9348
	golang.org/x/sync v0.0.0-20190412183630-56d357773e84
)
